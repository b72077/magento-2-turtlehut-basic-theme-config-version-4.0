This repo is for the Turtlehut basic theme.  It contains files for slick slideshow and config files for a new basic theme based on blank/luma.
To initialize a new turtlehut basic theme get the theme from Brian Draganski.  It needs a few files not included in this repository.
 