This readme and repo is under construction, talk to Brian to make sure it is ready.

This repo is for the Turtlehut basic theme.  It contains config files for a new basic theme based on Magento/blank, and adopted from Magento/luma.
Current bitbucket repo is git@bitbucket.org:b72077/magento-2.0-turtlehut-basic-theme-and-config-files.git
Changing luma files will have no effect once Turtlehut/basic is set as the theme in admin /panel (future repo should have no luma files)
Slick slideshow is included in the myslideshow.js file.  Refer to slick slideshow site to set up proper html.  
Currently need to include myslideshow.js in the home page content.  May change this feature to just get included automatically in home page xml?
.gitignore needs some work to optimize workflow and speed (it's already pretty quick though)

*******************************************************************************************************************************************
Notes on Setting up Magento 2 and css/less process
_
(need to change pwd in future. setup keys/passwords and other pains.)
refresh browser at correct URL set in hosts file on windows.  Not sure about Mac.  Google it and set it to 10.0.1.84 and corresponding url setup in vesta control panel for anvil server.
to add a domain name to the test server talk to Brian or Donnie or login to Vesta control panel, create a user, and then create a domain.
Install database table, 
Install Magento 2 (needs composer installed and grunt)
Install this repo.
Change theme in admin panel to Turtlehut/basic

*******************************************************************************************************************************************



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CSS/LESS modify process (working on simplifying instructions and setup before this will work):
_
Make your css/less changes in /web/css/source/_turtlehut.less file
use git bash or aptana terminal on local computer (may need to configure this and have keys added to repo)
git add --all
git commit -m 'message'
git push git@bitbucket.org:b72077/magento-2.0-turtlehut-basic-theme-and-config-files.git
Switch to ssh and login to 10.0.1.84 with your site username and password
on the anvil server ssh just run 'sh ./deploystyle.sh' and enter password (check for css/less errors here) 
refresh browser at correct url
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Edit footer links in Magento_Theme/layout/default.xml they are currently set to display in a table like format that is responsive.  
If you want to modify number of columns and rows, modify _turtlhut.less and myfooter.js  just adjust the variables accordingly (may automate this process)
There should be some helpful comments in there otherwise just mimic the temp links.


/////////////////////////////////////////////////////
DO NOT DO THE FOLLOWING THINGS:
do not edit anything in app/design/frontend/Magento/
more to come...                                                   
/////////////////////////////////////////////////////                                                                         
                                                                                  
///////////////////////////////////////////////////////////////////////////////////
Customizing and updating javascript/jquery process:

add/modify custom js to the web/js folder in your theme
if adding a new js file you need to modify deploysh.sh to rm the file, then cp the file
push the changes
switch to Anvil ssh
sh ./deployjs.sh
/////////////////////////////////////////////////////////////////////////////////             

Custom xml overrides
///////////////////////////////////////////////////////////////////////////////
currently overriding /cms/layout/cms_index_index.xml to load the slideshow only on the home page.  
Probably should add manually to the admin panel custom xml
cant' figure how to addjs via admin panel.  Seems to be no docs on this.
////////////////////////////////////////////////////////////////////////////////


 